<?php
namespace App\BookTitle\BookTitle;
use App\Model\Database as DB;


class BookTitle extends DB{

    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent::__construct();
    }

    /*
     public function index(){

        echo $this->id."<br>";
        echo $this->book_title."<br>";
        echo $this->author_name."<br>";

    }
    */

    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists('book_title',$postVariableData)){
            $this->book_title = $postVariableData['book_title'];
        }
        if(array_key_exists('author_name',$postVariableData)){
            $this->author_name = $postVariableData['author_name'];
        }
    }

    public function store(){
        $arrData = array($this->book_title, $this->author_name);
        $sql = "Insert INTO book_title(book_title, author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if($result)
            Message::getMessage("Success! Data has been inserted successfully :)");
        else
            Message::getMessage("Failed! Data has not been inserted successfully :(");

        Utility::redirect('create.php'); // redirect korte hobe create.php te tai utility.php use korechi //

    }//end of store method


}


$objBooktitle = new Booktitle();

